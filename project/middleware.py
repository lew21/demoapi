from django.http import HttpResponse


def AllowAllOrigins(get_response):
    def middleware(request, *args, **kwargs):
        resp = get_response(request, *args, **kwargs)
        resp['access-control-allow-origin'] = request.META['HTTP_ORIGIN'] if 'HTTP_ORIGIN' in request.META else '*'
        resp['access-control-allow-methods'] = resp['Allow'] if 'Allow' in resp else 'GET, HEAD, OPTIONS'
        resp['access-control-allow-headers'] = 'content-type'
        resp['access-control-expose-headers'] = 'allow, vary, location, www-authenticate, access-control-allow-origin, access-control-expose-headers, access-control-allow-methods'
        resp['www-authenticate'] = 'Basic'
        if request.method == 'OPTIONS':
            resp.status_code = 200
        return resp
    return middleware


apibrowser = """<!DOCTYPE html><html><head><meta charset=utf-8><meta name=viewport content="width=device-width,initial-scale=1"><title>APIBrowser</title><link href=https://apibrowser.storage.googleapis.com/static/css/app.568f5941d42567ef2a65c5cdb7b32463.css rel=stylesheet integrity="sha256-wu9idIWIq6GpemTy2UjJyALKwWzhx3asHU+SG2udyEE=" crossorigin=anonymous></head><body><div id=app></div><script type=text/javascript src=https://apibrowser.storage.googleapis.com/static/js/app.9f8edfd9b0e1afb81473.js integrity="sha256-Sdgc+hP8zDdwQwDYypvPePGGs3m7Auqw9Qs9uxOwCoQ=" crossorigin=anonymous></script></body></html>"""


def APIBrowserMiddleware(get_response):
    def middleware(request, *args, **kwargs):
        if request.method == 'GET' and 'text/html' in request.META['HTTP_ACCEPT']:
            return HttpResponse(apibrowser)

        return get_response(request, *args, **kwargs)
    return middleware
