set -e
python3 -m venv .upenv
. .upenv/bin/activate
pip install .[MySQL,PostgreSQL]
pip uninstall --yes dj12-project
pip freeze > requirements.txt
rm -Rf .upenv
