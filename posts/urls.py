from django.conf.urls import url, include

from rest_framework_nested import routers

from . import views

router = routers.DefaultRouter()
router.register(r'posts', views.PostsViewSet)

post_router = routers.NestedSimpleRouter(router, r'posts', lookup='post')
post_router.register(r'comments', views.CommentsViewSet, base_name='post-comment')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(post_router.urls)),
]
