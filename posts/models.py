from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=150)
    content = models.TextField()
    time = models.DateTimeField(auto_now_add=True)

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    content = models.TextField()
    time = models.DateTimeField(auto_now_add=True)
