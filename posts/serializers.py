from rest_framework import serializers
from rest_framework_nested import relations

from .models import Post, Comment


class PostSerializer(serializers.HyperlinkedModelSerializer):
    comments_url = serializers.HyperlinkedIdentityField('post-comment-list', lookup_url_kwarg='post_pk', read_only=True)

    class Meta:
        model = Post
        fields = ('url', 'title', 'content', 'time', 'comments_url')


post_lookup_kwargs = {
    'post_pk': 'post__pk',
}


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Comment
        fields = ('url', 'post_url', 'content', 'time')

    url = relations.NestedHyperlinkedIdentityField('post-comment-detail', parent_lookup_kwargs=post_lookup_kwargs)
    post_url = serializers.HyperlinkedRelatedField('post-detail', read_only=True, source='post')

    def create(self, validated_data):
        post = self.post
        return self.post.comment_set.create(**validated_data)
