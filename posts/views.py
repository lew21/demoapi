from rest_framework import viewsets

from .models import Post, Comment
from .serializers import PostSerializer, CommentSerializer

class PostsViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

class CommentsViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    @property
    def post_(self):
        return Post.objects.get(id=self.kwargs['post_pk'])

    def perform_create(self, serializer):
        serializer.post = self.post_
        super().perform_create(serializer)
